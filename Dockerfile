FROM python:3-alpine

ENV user root
ENV password 123
ENV email root@ubuntu.ru

WORKDIR /code
RUN mkdir db

COPY . /requirements.txt /code
RUN pip install -r requirements.txt

COPY . /code

EXPOSE 8000
volume ["/code/db"]

CMD sh init.sh && python manage.py runserver 0.0.0.0:8000
